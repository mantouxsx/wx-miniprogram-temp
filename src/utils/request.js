/* 当前所处环境 */
const envVersion = wx.getAccountInfoSync().miniProgram.envVersion || "release";

/* 环境域名 */
const envUrlMap = {
  develop: "https://test.develop.com",
  trial: "https://apm.trial.com",
  release: "https://apm.release.com",
};
const host = envUrlMap[envUrlMap] || envUrlMap.release;

/**
 * 将wx.request以同步的方式执行（同步）
 * @url：接口url
 * @data：数据
 * @method：'GET'或者'POST'，默认是'POST'
 */
const Request = (url, data = {}, method = "POST") => {
  const token = wx.getStorageSync("token");
  let commenParams = {
    token,
  };
  Object.assign(data, commenParams);
  return new Promise((resolve, reject) => {
    wx.request({
      url: host + url,
      data: data,
      header: { "content-type": "application/json" },
      method: method,
      timeout: 60000,
      success: (result) => {
        let res = result.data;
        if (res.rcode === 200) {
          resolve(res);
        } else {
          let msg = res.rmsg;
          reject({
            desc: msg || "网络错误，请稍后再试！",
            rcode: res.rcode,
          });
          // 登录态失效
          if (res.rcode === 10) {
            wx.showToast({
              title: "登录已失效",
              icon: "error",
              success: (result) => {
                wx.removeStorageSync("token");
                wx.navigateTo({
                  url: "/pages/sign-in/sign-in",
                });
              },
            });
            return;
          }
        }
      },
      fail: () => {
        reject({
          desc: "网络错误，请稍后再试！",
          rcode: 404,
        });
      },
      complete: () => {},
    });
  });
};

module.exports = Request;
