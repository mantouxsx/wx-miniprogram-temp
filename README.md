# wx-miniprogram-temp

### 介绍

微信小程序简易开发模板，包含登录、组件引用等。适合第一次了解微信小程序开发的朋友直接下载使用，在此模板上进行开发，或者使用该模板的一些页面和组件。

### 软件架构

#### 目录结构



#### 常用组件和封装工具

1. 自定义导航栏；
2. request封装和环境配置；

#### 常用页面

1. 登录页面

### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

### 使用说明

#### 自定义导航栏

> 参考 https://developers.weixin.qq.com/community/develop/article/doc/00068aec7941f8f57509794be54413

### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
